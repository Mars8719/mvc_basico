/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.patrones_ejercicio_mvc_completo;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author 57301
 */
public class CelularesControlador implements ActionListener {

    private CelularesVista vista;
    private CelularesModelo modelo;

    public CelularesControlador(CelularesVista vista, CelularesModelo modelo) {
        this.vista = vista;
        this.modelo = modelo;
        vista.getBtnAgregar().addActionListener(this);
        vista.getBtnClonar().addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == vista.getBtnAgregar()) {
            //actulizando los valores del modelo
            modelo.setMarca(vista.getTxtMarca().getText());
            modelo.setModelo(vista.getTxtModelo().getText());
            modelo.setPrecio(Double.parseDouble(vista.getTxtPrecio().getText()));

            //Inicializando el mensaje en vacio si es null (la primera vez)
            if (modelo.getMensaje() == null) {
                modelo.setMensaje("");
            }

            //Armando mensaje completo, lo que habia, nueva linea y el nuevo mensaje
            String mensajeFinal = modelo.getMensaje() + "\n"
                    + "El Celular es "
                    + "Marca: " + modelo.getMarca() + ", "
                    + "Modelo: " + modelo.getModelo() + ", "
                    + "Precio: " + modelo.getPrecio();

            //Actualizando el modelo con el nuevo mensaje
            modelo.setMensaje(mensajeFinal);

            //actualizando la vista
            vista.getTxtMensaje().setText(mensajeFinal);
        } else if (e.getSource() == vista.getBtnClonar()) {
            CelularesModelo clonado = modelo.clone();
            System.out.println("clonado: "+clonado.toString());
        }
    }

}
