/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.patrones_ejercicio_mvc_completo;

/**
 *
 * @author 57301
 */
public class CelularesModelo {

    private String marca;
    private String modelo;
    private Double precio;

    private String mensaje;

    private CelularesModelo(CelularesModeloBuilder builder) {
        marca = builder.marca;
        modelo = builder.modelo;
        precio = builder.precio;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
    
    @Override
     public CelularesModelo clone() {
        CelularesModeloBuilder builder = new CelularesModeloBuilder();
        CelularesModelo cm = builder.conMarca(marca)
                .conModelo(modelo).conPrecio(precio).build();
        return cm;
    }

    @Override
    public String toString() {
        return "CelularesModelo{" + "marca=" + marca + ", modelo=" + modelo + ", precio=" + precio + ", mensaje=" + mensaje + '}';
    }
     

    public static final class CelularesModeloBuilder {

        private String marca;
        private String modelo;
        private Double precio;
        
        public CelularesModeloBuilder conMarca(String v){
            marca = v;
            return this;
        }
        
        public CelularesModeloBuilder conModelo(String v){
            modelo = v;
            return this;
        }
        
        public CelularesModeloBuilder conPrecio(Double v){
            precio = v;
            return this;
        }
        
         public CelularesModelo build() {
//            if (marca == null) {
//                throw new IllegalStateException("Se requiere la marca");
//            }
//            if (modelo == null) {
//                throw new IllegalStateException("Se requiere el modelo");
//            }
//            if (precio==null || precio <0){
//                throw new IllegalStateException("Se requiere el precio y que sea mayor o igual a cero");
//            }
            return new CelularesModelo(this);
        }
    }

}
