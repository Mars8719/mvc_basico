/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.patrones_ejercicio_mvc_completo;

/**
 *
 * @author 57301
 */
public class Usuario {
    private String nombre;
    private static Usuario usuario;

    private Usuario(String nombre) {
        this.nombre = nombre;
    }
    
    public Usuario getInstance(String nombre){
        if (usuario==null){
            usuario = new Usuario(nombre);
        }
        return usuario;
    }

    public String getNombre() {
        return nombre;
    }
    
}
