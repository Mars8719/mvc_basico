/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package com.mycompany.patrones_ejercicio_mvc_completo;

/**
 *
 * @author 57301
 */
public class Patrones_ejercicio_mvc_completo {

    public static void main(String[] args) {
        CelularesVista vista = new CelularesVista();
        CelularesModelo modelo = new CelularesModelo.CelularesModeloBuilder().build();
        CelularesControlador controlador = new CelularesControlador(vista, modelo);
        vista.setVisible(true);
    }
}
